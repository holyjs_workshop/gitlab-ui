import description from './tab.md';
import examples from './examples';

export default {
  description,
  examples,
  bootstrapComponent: 'b-tab',
  followsDesignSystem: true,
};
